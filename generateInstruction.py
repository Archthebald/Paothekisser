#!/usr/bin/env python3
from PIL import Image
import numpy as np
import csv
import sys
import tkinter
from tkinter.filedialog import askopenfilename
import json

#Select image to process
if len(sys.argv) > 1:
    filename = sys.argv[1]
else:
    #Open dialog box to choose image from memory
    tkinter.Tk().withdraw() # we don't want a full GUI, so keep the root window from appearing
    filename = askopenfilename(initialdir = "./images",title = "Select Image",filetypes = (("image files","*.jpg *.png *.jpeg"),("all files","*.*"))) # show an "Open" dialog box and return the path to the selected file

image = Image.open("%s"%(filename)).convert("L")
imageArray = np.array(image)

#INSTRUCTION variables
DIRECTION = ["R", "L", "D","U"]
INTENSITY = [0, 1, 2, 3, 4, 5, 6]
POWER = 0

#Number of times row is traversed
MAX = 1

#Create empty instructionArray
rows = imageArray.shape[0]
cols = imageArray.shape[1]
instructionArray = np.empty([rows * MAX,cols],dtype="object_") #because row is not 1:1 with pixel

#Instruction generation variables
changeLine = True
path = DIRECTION[0]
intensity = INTENSITY[0]
power = POWER

#Structures to reverse rows
counter = 1
stack = []
daq = []

print("Generating Instruction Set...")
for x in range(rows*MAX):
    #Detect line change
    changeLine = not changeLine
    path = DIRECTION[0]

    for y in range(cols):
        #Determine DIRECTION based on line and endpoint
        if y == cols-1 and MAX != 1:
            path = DIRECTION[2]
        elif (y == cols-1 and not changeLine) and MAX == 1:
            path = DIRECTION[2]
        elif (y == 0 and changeLine) and MAX == 1:
            path = DIRECTION[2]
        elif changeLine:
            path = DIRECTION[1]

        #If 1st iteration of row, extract from image as normal
        if counter == 1:
            newX = int(x/MAX) # since x can get to MAX * rows, to stay in bounds of imageArray
            #Calculate INTENSITY and POWER
            if imageArray[newX,y] >= 226 and imageArray[newX,y] <= 255:       # white
                intensity = INTENSITY[0]
                power = POWER
            elif imageArray[newX,y] >= 186 and imageArray[newX,y] <= 225:     # light grey
                intensity = INTENSITY[1]
                power = int(not POWER)
            elif imageArray[newX,y] >= 146 and imageArray[newX,y] <= 185:     # medium grey
                intensity = INTENSITY[2]
                power = int(not POWER)
            elif imageArray[newX,y] >= 106 and imageArray[newX,y] <= 145:     # grey
                intensity = INTENSITY[3]
                power = int(not POWER)
            elif imageArray[newX,y] >= 66 and imageArray[newX,y] <= 105:      # dim grey
                intensity = INTENSITY[4]
                power = int(not POWER)
            elif imageArray[newX,y] >= 26 and imageArray[newX,y] <= 65:       # dark grey
                intensity = INTENSITY[5]
                power = int(not POWER)
            elif imageArray[newX,y] >= 0 and imageArray[newX,y] <= 25:        # black
                intensity = INTENSITY[6]
                power = int(not POWER)

            instruction = path + str(intensity) + str(power)
            instructionArray[x,y] = instruction
            stack.append(str(intensity) + str(power))

        #If an even iteration of row, pop partial instruction from stack1, add path, and push to queue
        #Then apply to array
        elif counter % 2 == 0:
            s = stack.pop()
            instruction = path + s
            daq.append(s)
            instructionArray[x,y] = instruction

        #If an odd iteration of row, pop partial instruction from stack2, add path, and push to stack
        #Then apply to array
        elif counter % 2 == 1:
            s = daq.pop()
            instruction = path + s
            stack.append(s)
            instructionArray[x,y] = instruction

    #Reverse other line to assist List concatenation for csv if MAX == 1
    if changeLine and MAX == 1:
        instructionArray[x] = instructionArray[x][::-1]

    #After counter reaches MAX, revert to 1
    if counter == MAX:
        counter = 0

    counter += 1
#end loop

#Flatten 2D array into 1D array, then convert to list for csv file
instructionArray = instructionArray.flatten()
myData = instructionArray.tolist()

myFile = open('INSTRUCTION_SET.csv', 'w', newline="")
with myFile:
    writer = csv.writer(myFile)
    writer.writerow(myData)

print("Instructions Generated")
image.show()

#JSON fields
width = str(rows)
height = str(cols)
data = {'dimensions':[{'width': width,'height': height}]}

nextFile = open('INSTRUCTION_SET.json', 'w')
with nextFile:
    json.dump(data, nextFile, indent=4)
