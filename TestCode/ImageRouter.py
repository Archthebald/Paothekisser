from PIL import Image
import numpy as np

#Uses image from my Desktop
image1 = Image.open("C:/Users/Nathan/Desktop/Lipton_Kermit.jpeg").convert("L")
image1arr = np.array(image1)

#Uses gradient image that shows grayscale variance
# image2 = Image.open(".images/grayscale-gradient.jpeg")
# image2arr = np.array(image2)

#print image arrays
# print(image1arr)
# print(image2arr)

#grayscale: 0 = black, 255 = white

#image2arr.shape --> (255, 255)

#Change the first 25 lines of image2 to white
# image2arr[:25] = 255 
# newimage2 = Image.fromarray(image2arr)
# newimage2.show()

#Method that checks for change in intensity and changes it  
# def changeIntensity(currentIntensity, newIntesity):
#     if currentIntensity != newIntesity:
#         global LASER_INTENSITY
#         LASER_INTENSITY = newIntesity
#         # print(LASER_INTENSITY)

# #image1arr.shape ---> (656, 979)
# rows = image1arr.shape[0]
# cols = image1arr.shape[1]

# #Create empty array to hold instructions (strings)
# instructionArray = np.empty([rows,cols],dtype="object_")        
        
# CHANGE_LINE = 1
# #Loop to build InstructionArray
# for x in range(0,rows):
#     LASER_DIRECTIONS = "R"
#     LASER_INTENSITY = 0
#     CHANGE_LINE = not CHANGE_LINE

#     for y in range(0,cols):
#         #Alternate left and right LASER paths down each row
#         if y == cols-1 and not CHANGE_LINE:
#             LASER_DIRECTIONS = "D"
#         elif y == 0 and CHANGE_LINE:
#             LASER_DIRECTIONS = "D" 
#         elif CHANGE_LINE:
#             LASER_DIRECTIONS = "L"
               
#         #Determine Intesity based on color ranges (white, gray, darkgray, black) <Modifiable>    
#         if image1arr[x,y] >= 236 and image1arr[x,y] <= 255:
#             changeIntensity(LASER_INTENSITY, 0)
#         elif image1arr[x,y] >= 136 and image1arr[x,y] <= 235:
#             changeIntensity(LASER_INTENSITY, 1)            
#         elif image1arr[x,y] >= 36 and image1arr[x,y] <= 135:
#             changeIntensity(LASER_INTENSITY, 2)            
#         else:
#             changeIntensity(LASER_INTENSITY, 3)

#         instruction =  str(LASER_INTENSITY) + LASER_DIRECTIONS
#         instructionArray[x,y] = instruction
        
# #InstructionArray includes [ LASER_INTENSITY, LASER_DIRECTION ] (More can be added)

# ###########################################
# #Test instructions at the end/start of line
# print(instructionArray[0,0], " : start of InstructionArray, path RIGHT, step # 1")    
# print(instructionArray[0,cols-2], " : part of 1st line, path RIGHT, step #",cols-1)
# print(instructionArray[0,cols-1], " : end of 1st line, path DOWN, step #",cols)
# print(instructionArray[1,0], " : start of 2nd line in array, path DOWN, step #",2*cols)
# print(instructionArray[1,2], " : part of 2nd line in array, path LEFT, step #",2*cols-1)
# print(instructionArray[1,cols-1], " : end of 2nd line in array, path LEFT, step #",cols+1)
# print(instructionArray[2,0], " : start of 3rd line in array, path RIGHT, step #",2*cols+1)
# print(image2arr)
# print(range(0,image2arr.shape[0]))

# for x in range(image2arr.shape[0] - 1):
#     for y in range(255):
#         print(image2arr[x,y])

print(0%2)
print(1%2)
print(2%2)
print(3%2)
