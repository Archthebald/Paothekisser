# #!/usr/bin/env python2
#
# import RPi.GPIO as gpio
# from time import sleep
# rot = 21
# pul = 20
#
# gpio.setmode(gpio.BCM)
# gpio.setup(pul, gpio.OUT)
# gpio.setup(rot, gpio.OUT)
#
# SPR = 12800
# step_count = SPR
# delay = 0.1
#
#
# for y in range(1):
#     for x in range(step_count):
#         gpio.output(pul, gpio.HIGH)
#         sleep(delay)
#         gpio.output(pul,gpio.LOW)
#         sleep(delay)
#
#
#     gpio.output(rot, gpio.HIGH)
#
#     sleep(delay)
#     for x in range(step_count):
#         gpio.output(pul, gpio.HIGH)
#         sleep(delay)
#         gpio.output(pul, gpio.LOW)
#         sleep(delay)
#
#     gpio.output(rot, gpio.LOW)
#     sleep(delay)
#

import RPi.GPIO as gpio
from time import sleep
rot = 21
pul = 20

gpio.setmode(gpio.BCM)
gpio.setup(pul, gpio.OUT)
gpio.setup(rot, gpio.OUT)

SPR = 12800
step_count = SPR


for y in range(1):
    for x in range(step_count):
        sleep(delay)
        gpio.output(pul,gpio.LOW)
    
    
    gpio.output(rot, gpio.HIGH)

    for x in range(step_count):
        gpio.output(pul, gpio.HIGH)
        sleep(delay)
        gpio.output(pul, gpio.LOW)
        sleep(delay)
    
    gpio.output(rot, gpio.LOW)
    sleep(delay)

