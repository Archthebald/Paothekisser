# Paothekisser

There are three abstracted parts to this hardware project.

### Hardware

  This is the physical configuration of the motors and power supply.
  The setup will be completed and tested with the Controller software
  to ensure bug free operation of the laser.

### Routing

  This is the most critical part of the project.  This decodes an image into
  and instruction set for the controller to follow and thus the laser to follow.

  todo: Nathan- Put a summary of the output language of the routing software to
  here, and an example of a full instruction set.

  At the beginning of a file set, there will be the command
  "'Begin Instructions'\n"

  and at the end of the instruction string

  "\n'End Instructions'"

  Not the new line characters separating it from the fill string of instructions.

  Also, lines beginning with # will be ignored by the Controller and will be
  used for comments.

### Controller
  This program is the interface to the hardware and gives out low level logic
  commands to different hardware to operate the laser.  The Controller should
  have the ability to do free entry commands, or follow a string of commands
  a routing file.  There doesn't need to be a GUI for this project, though a
  window with the current state of the program is welcome, time permitting.
  This program is mostly string parsing and hardware manipulation.  Do this part
  of the code with as few try-catch statements as possible.  Good programming
  will allow you to eliminate the need for any.  If you attempt a GUI, python
  has a very easy to use library for that sort of thing.
