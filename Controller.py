#!/usr/bin/env python3
# Imports
import pprint
import RPi.GPIO as gpio
import csv
import json
import sys
from time import sleep

# Global variables
global xPosition
global yPosition
global xDir
global xPul
global yDir
global yPul
global SPR
global Speed_multiplier
global resolution
global Steps
global height
global width
global settingsJson
global presets

settingsJson = json.load(open('./INSTRUCTION_SET.json'))
dimensions = settingsJson.get("dimensions",None)
# pp = pprint.PrettyPrinter(indent=4)
# pp.pprint(settingsJson)

Steps = 5
speed_multiplier = 1
resolution = 1
SPR = 12800
xPosition = 0
yPosition = 0
xDir = 19
xPul = 26
yDir = 16
yPul = 20
presets = [0.0001, 0.0004, 0.0009, 0.0015, 0.0017, 0.0028, 0.003]


height =  int (dimensions[0]["height"])
width = int (dimensions[0]["width"])

# position sensor variables
x_axis_zero = 0
y_axis_zero = 0
fan_active = True

# laser states
active = False
active_time_ms = 0
temp_celc = 0
laserCtrl = 23

# pin stuff
gpio.setmode(gpio.BCM)
gpio.setup(xPul, gpio.OUT)
gpio.setup(xDir, gpio.OUT)
gpio.setup(yPul, gpio.OUT)
gpio.setup(yDir, gpio.OUT)
gpio.setup(laserCtrl, gpio.OUT)
gpio.setup(x_axis_zero, gpio.IN)
gpio.setup(y_axis_zero, gpio.IN)
# functions for movement

def Bobross():

        global Steps
        global height
        global width
        global presets

        # Center_laser()
        Move('L',int(width/2)*Steps, presets[1])
        Move('U', int(height/2)*Steps,presets[1])

        print(int(int(height)/2)*Steps)
        print(int(int(width)/2)*Steps)
        with open('INSTRUCTION_SET.csv', 'r') as csvDataFile:
            csvReader = csv.reader(csvDataFile)
            for row in csvReader:
                for val in row:
                    Move(val[0], Steps, presets[ int( val[1] ) ] )
                    print(val[1])

        sys.exit(0)

def Center_laser():

   # Global variables
    global xPosition
    global yPosition
    global xDir
    global xPul
    global yDir
    global yPul
    global SPR
    global Speed_multiplier

    if(xPosition > 0):
        Move('L', xPosition, 0.0001)
    elif(xPosition < 0):
        Move('R', (xPosition * -1), 0.0001)
    xPosition = 0
    if(yPosition > 0):
        Move('D', yPosition, 0.0001)
    elif(yPosition < 0):
        Move('U', (yPosition * -1), 0.0001)
    yPosition = 0

def Move(direction, steps, delay):

	global xPosition, yPosition, xDir,xPul,yDir,yPul, SPR, Speed_multiplier
	if (direction == 'R'):
		# set rotation to counter clockwise
		gpio.output(xDir, gpio.HIGH)
		# rotate x motor
		for x in range(steps):
                    if(xPosition < SPR/2):
                        gpio.output(xPul, gpio.HIGH)
                        sleep(delay)
                        gpio.output(xPul, gpio.LOW)
                        sleep(delay)
                        xPosition += 1
                        # set new position
	elif (direction == 'L'):
		# set rotation to clockwise
		gpio.output(xDir, gpio.LOW)
		# rotate x motor
		for x in range(steps):
                    if (xPosition > (-1 * (SPR/2))):
                        gpio.output(xPul, gpio.HIGH)
                        sleep(delay)
                        gpio.output(xPul, gpio.LOW)
                        sleep(delay)
                        # set new position
                        xPosition -= 1
	elif (direction == 'D'):
		# set rotation to counter clockwise
		gpio.output(yDir, gpio.HIGH)
		# rotate y motor
		for x in range(steps):
                    if(yPosition > -2000):
                        gpio.output(yPul, gpio.HIGH)
                        sleep(delay)
                        gpio.output(yPul, gpio.LOW)
                        sleep(delay)
                        # set new position
                        yPosition -= 1
	elif (direction == 'U'):
		# set rotation to clockwise
		gpio.output(yDir, gpio.LOW)
		# rotate y motor
		for x in range(steps):
                    if(yPosition < 2000):
                        gpio.output(yPul, gpio.HIGH)
                        sleep(delay)
                        gpio.output(yPul, gpio.LOW)
                        sleep(delay)
                        # set new position
                        yPosition += 1

def Bounds ():
    global height, width, Steps

    Move('L',int(width/2)*Steps, presets[1])
    Move('U', int(height/2)*Steps,presets[1])

    for _ in range(4):
        Move('R', width*Steps, presets[1])
        Move('D', height*Steps, presets[1])
        Move('L', width*Steps, presets[1])
        Move('U', height*Steps, presets[1])

    Move('R',int(width/2)*Steps, presets[1])
    Move('D', int(height/2)*Steps,presets[1])


        
def Find_center():

    # Global variables
    global xPosition
    global yPosition
    global xDir
    global xPul
    global yDir
    global yPul
    global SPR
    global Speed_multiplier

    gpio.output(xDir, gpio.LOW)
    for x in range(0.4 * SPR):
        if(gpio.input(x_axis_zero) == GPIO.LOW):
            break
        gpio.output(xPul, gpio.HIGH)
        sleep(delay)
        gpio.output(xPul, gpio.LOW)
        sleep(delay)
    gpio.output(xDir, gpio.HIGH)
    for x in range(0.8 * SPR):
        if(gpio.input(x_axis_zero) == GPIO.LOW):
            break
        gpio.output(xPul, gpio.HIGH)
        sleep(delay)
        gpio.output(xPul, gpio.LOW)
        sleep(delay)
    gpio.output(yDir, gpio.LOW)
    for x in range(0.4 * SPR):
        if(gpio.input(y_axis_zero) == GPIO.LOW):
            break
        gpio.output(yPul, gpio.HIGH)
        sleep(delay)
        gpio.output(yPul, gpio.LOW)
        sleep(delay)
    gpio.output(yDir, gpio.HIGH)
    for x in range(0.8 * SPR):
        if(gpio.input(y_axis_zero) == GPIO.LOW):
            break
        gpio.output(yPul, gpio.HIGH)
        sleep(delay)
        gpio.output(yPul, gpio.LOW)
        sleep(delay)
    if(gpio.input(x_axis_zero) == GPIO.LOW):
        xPosition = 0
    if(gpio.input(y_axis_zero) == GPIO.LOW):
        yPosition = 0
    #(if centered) if(gpio.input(x_axis_zero) == 0 and gpio.input(y_axis_zero) == 0):

running = True
while running:
    instruction = input()

    if instruction == ('d' or 'D'):
        Move('D', 200, presets[0])
    elif instruction == ('u' or 'U'):
        Move('U', 200, presets[0])
    elif instruction == ('r' or 'R'):
        Move('R', 200, presets[0])
    elif instruction == ( 'l' or 'L'):
        Move('L', 200, presets[0])
    elif instruction == 'q':
        running = False
    elif instruction =='c':
        Center_laser()
    elif instruction == 'b':
        Bounds()
Bobross()
gpio.cleanup()
